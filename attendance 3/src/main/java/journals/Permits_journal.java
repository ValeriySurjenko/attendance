package journals;


import java.util.*;
import java.util.List;
import additional.Permit;
import javax.faces.context.FacesContext;



public class Permits_journal {
    private List<additional.Permit> premits;
    private int page;
    
    private String filter_regnum;
    private Date filter_datereg;
    private int filter_datereg_direction;
    private int filter_birth_direction;
    private String filter_age;
    private Date filter_birth;
    private String filter_status;
    //******************************************

    public int getFilter_birth_direction() {
        return filter_birth_direction;
    }

    public void setFilter_birth_direction(int filter_birth_direction) {
        this.filter_birth_direction = filter_birth_direction;
    }

    public int getFilter_datereg_direction() {
        return filter_datereg_direction;
    }

    public void setFilter_datereg_direction(int filter_datereg_direction) {
        this.filter_datereg_direction = filter_datereg_direction;
    }

    public String getFilter_regnum() {
        return filter_regnum;
    }

    public void setFilter_regnum(String filter_regnum) {
        this.filter_regnum = filter_regnum;
    }

    public Date getFilter_datereg() {
        return filter_datereg;
    }

    public void setFilter_datereg(Date filter_datereg) {
        this.filter_datereg = filter_datereg;
    }

    public String getFilter_age() {
        return filter_age;
    }

    public void setFilter_age(String filter_age) {
        this.filter_age = filter_age;
    }

    public Date getFilter_birth() {
        return filter_birth;
    }

    public void setFilter_birth(Date filter_birth) {
        this.filter_birth = filter_birth;
    }

    public String getFilter_status() {
        return filter_status;
    }

    public void setFilter_status(String filter_status) {
        this.filter_status = filter_status;
    }
    
    //******************************************
    public boolean id_comp(String num1,int num2)
    {
        if(num1.isEmpty()==true) return true;
        int nm=Integer.parseInt(num1);
        if(nm==0)return true;
        if(nm==num2)return true;
        return false;
                
    }
    public boolean date_comp(Date d1,Date d2,int comp)  
    {
       if (d1==null){return true;}
       switch (comp)
       {
           case 0:
               if ((d1.getDay()==d2.getDay())&&(d1.getMonth()==d2.getMonth())&&(d1.getYear()==d2.getYear())){return true;}
               break;
           case 1:
               if (d1.after(d2)){return true;}
               break;
           case 2:
               if (d1.before(d2)){return true;}
               break;

       }
        return false;
    }
    public List<Permit> getPremits() {
        return premits;
    }

    public void setPremits(List<Permit> premits) {
        this.premits = premits;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
    
    //******************************************
    public Permits_journal() {
        
       Calendar d1;
       d1=new GregorianCalendar(2011,5,2);
       Date d2=d1.getTime();
       premits=new ArrayList<Permit>();
        


         
         premits.add(new Permit("Ворошиловский","Кондратов Семен Никифорович",3,125,"ВЫдача путевки",new GregorianCalendar( 2011,01,23).getTime(),new GregorianCalendar(2011,07,12).getTime()));
         premits.add(new Permit("Ворошиловский","Шапошникова Марья Петровна",2,126,"Подписан логовор",new GregorianCalendar(2011,11,20).getTime(),new GregorianCalendar(2012,07,12).getTime()));
         premits.add(new Permit("Ворошиловский","Сахоров Аркадий Ярославович",3,127,"Архив",new GregorianCalendar(2011,03,3).getTime(),new GregorianCalendar(2011,05,1).getTime()));
    }
}
