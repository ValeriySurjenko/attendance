/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package additional;

import java.util.Date;

/**
 *
 * @author admin
 */

public class Permit {
    private String area;
    private String fio;
    private int age;
    private Date birth;
    private int permit_number;
    private Date permit_date;
    private String status;
    //***********************************************

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public int getPermit_number() {
        return permit_number;
    }

    public void setPermit_number(int permit_number) {
        this.permit_number = permit_number;
    }

    public Date getPermit_date() {
        return permit_date;
    }

    public void setPermit_date(Date permit_date) {
        this.permit_date = permit_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Permit(String area, String fio, int age, int permit_number, String status) {
        this.area = area;
        this.fio = fio;
        this.age = age;
        this.permit_number = permit_number;
        this.status = status;
    }
     public Permit(String area, String fio, int age, int permit_number, String status,Date birth,Date reg) {
        this.area = area;
        this.fio = fio;
        this.age = age;
        this.permit_number = permit_number;
        this.status = status;
        this.birth=birth;
        this.permit_date=reg;
    }
     //***********************************************
    
    public Permit() {
       
    }
}
