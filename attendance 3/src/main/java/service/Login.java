/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.faces.context.FacesContext;

/**
 *
 * @author admin
 */
public class Login {
    private String curpage;
    private String username;
    private String pass;
    private boolean logined;
    //***************************************

    public String getCurpage() {
        return curpage;
    }

    public void setCurpage(String curpage) {
        this.curpage = curpage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isLogined() {
        return logined;
    }

    public void setLogined(boolean logined) {
        this.logined = logined;
    }
    //***************************************
    public boolean compare(String val1,String val2)
    {
        if(val2.length()==0||val1.length()==0)return true;
        return val1.equalsIgnoreCase(val2);
               
    }
    public String check_user()
    {
        logined=true;
        try
        {
        FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        }
        catch(Exception ex)
        {
                        
        }
        return "/faces/index.xhtml";
    }
    public String check_logined()
    {
        if(logined==false)
        {
            try
            {
                FacesContext.getCurrentInstance().getExternalContext().redirect("faces/login.xhtml");
            }
            catch(Exception ex)
            {
                        
            }
                    
            return"faces/login.xhtml";
        }
        return "";
    }
    //***************************************
    public Login() 
    {
        logined=false;
        curpage="/welcome.xhtml";
    }
}
