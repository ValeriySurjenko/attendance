/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package additional;

/**
 *
 * @author admin
 */
public class AgeGroup {
    private Long id;
    private String title;
    private Float age_bound_left;
    private Float age_bound_right;
    //-----------------------------------
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getAge_bound_left() {
        return age_bound_left;
    }

    public void setAge_bound_left(Float age_bound_left) {
        this.age_bound_left = age_bound_left;
    }

    public Float getAge_bound_right() {
        return age_bound_right;
    }

    public void setAge_bound_right(Float age_bound_right) {
        this.age_bound_right = age_bound_right;
    }
    //-----------------------------------

    public AgeGroup() {
    }

    public AgeGroup(String title, Float age_bound_left, Float age_bound_right) {
        this.title = title;
        this.age_bound_left = age_bound_left;
        this.age_bound_right = age_bound_right;
    }

}
