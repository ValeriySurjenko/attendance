/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package additional;

import java.util.Date;

/**
 *
 * @author admin
 */

public class Permit {
    private String area;
    private String fio;
    private int age;
    private Date birth;
    private String birthstr;
    private int permit_number;
    private Date permit_date;
    private String permit_datestr;
    private String status;
    //***********************************************

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirth() {
        birthstr=this.birth.toLocaleString();
        return birth;
        
    }

    public void setBirth(Date birth) {
        this.birth = birth;
        birthstr=this.birth.toLocaleString();
    }

    public String getBirthstr() {
        birthstr=this.birth.toLocaleString();
        return birthstr;
    }

    public void setBirthstr(String birthstr) {
        this.birthstr = birthstr;
    }
    
    public int getPermit_number() {
        return permit_number;
    }

    public void setPermit_number(int permit_number) {
        this.permit_number = permit_number;
    }

    public Date getPermit_date() {
        permit_datestr=this.permit_date.toLocaleString();
        return permit_date;
    }

    public void setPermit_date(Date permit_date) {
        this.permit_date = permit_date;
        permit_datestr=this.permit_date.toLocaleString();
    }

    public String getPermit_datestr() {
        permit_datestr=this.permit_date.toLocaleString();
        return permit_datestr;
    }

    public void setPermit_datestr(String permit_datestr) {
        this.permit_datestr = permit_datestr;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Permit(String area, String fio, int age, int permit_number, String status) {
        this.area = area;
        this.fio = fio;
        this.age = age;
        this.permit_number = permit_number;
        this.status = status;
    }
     public Permit(String area, String fio, int age, int permit_number, String status,Date birth,Date reg) {
        this.area = area;
        this.fio = fio;
        this.age = age;
        this.permit_number = permit_number;
        this.status = status;
        this.birth=birth;
        this.permit_date=reg;
    }
     //***********************************************
    
    public Permit() {
       
    }
}
