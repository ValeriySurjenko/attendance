package org.itx.jbalance.equeue;


import java.rmi.RMISecurityManager;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginContext;

import org.itx.jbalance.l2_api.services.AttendanceService;


public class LookupHelper  {
    
    
	/** The default JNDI provider URL: {@value #DEFAULT_PROVIDER_URL}. */
    public static final String DEFAULT_PROVIDER_URL = "jnp://localhost:1099";

    /** The fully qualified classname of the default unauthenticated initial context factory. */
    public static final String DEFAULT_JNDI_INITIAL_CONTEXT_FACTORY = "org.jnp.interfaces.NamingContextFactory";

    /** The fully qualified classname of the initial context factory required to access secured JNDI resources. */
    public static final String JNDI_LOGIN_INITIAL_CONTEXT_FACTORY = "org.jboss.security.jndi.JndiLoginInitialContextFactory";

    private String providerUrl = DEFAULT_PROVIDER_URL;
    private String initialContextFactory = DEFAULT_JNDI_INITIAL_CONTEXT_FACTORY;
    private String securityPrincipal;
    private String securityCredentials;
    private Context remoteContext;

    /**
     * Sets the URL for the remote naming context provider.  Changes to this
     * will cause any open remote connections to be closed.  The default value
     * is {@value #DEFAULT_PROVIDER_URL}, which is stored in the constant
     *
     * @param providerUrl The URL for the remote naming context provider
     * {@link #DEFAULT_PROVIDER_URL}.
     */
    public void setProviderUrl(String providerUrl) {
        close();
        this.providerUrl = providerUrl;
    }

    /**
     * Sets the fully qualified classname of the initial context factory to use
     * when establishing a connection to the remote naming context.  Changes to
     * this will cause any open remote connections to be closed.  The default
     * value is {@value #DEFAULT_JNDI_INITIAL_CONTEXT_FACTORY}, which is stored
     * in the constant {@link #DEFAULT_JNDI_INITIAL_CONTEXT_FACTORY}.  Since
     * access to the remote API resources are restricted to authenticated users
     * having particular roles, it is recommended that callers specify the value
     *
     * @param initialContextFactory The fully qualified classname of the initial
     * context factory to use
     * {@value #JNDI_LOGIN_INITIAL_CONTEXT_FACTORY} (stored in the constant
     * {@link #JNDI_LOGIN_INITIAL_CONTEXT_FACTORY}) to use the JBoss security
     * extensions to authenticate and securely access the remote API resources.
     */
    public void setInitialContextFactory(String initialContextFactory) {
        close();
        this.initialContextFactory = initialContextFactory;
    }

    /**
     * Sets the username to use when establishing a connection to the remote
     * naming context.  This is the value of the {@code
     * Context.SECURITY_PRINCIPAL} property specified when instantiating a new
     * initial context.
     *
     * @param securityPrincipal The username to use to connect to the remote
     * naming context
     */
    public void setSecurityPrincipal(String securityPrincipal) {
        close();
        this.securityPrincipal = securityPrincipal;
    }

    /**
     * Sets the password to use when establishing a connection to the remote
     * naming context.  This is the value of the {@code
     * Context.SECURITY_CREDENTIALS} property specified when instantiating a new
     * initial context.
     *
     * @param securityCredentials The password to use to connect ot the remote
     * naming context
     */
    public void setSecurityCredentials(String securityCredentials) {
        close();
        this.securityCredentials = securityCredentials;
    }

    /**
     * Returns a reference to the remote context, initializing it if necessary
     *
     * @return A reference to the remote context
     *
     * @throws NamingException If there is an error initializing the remote
     * context
     */
    private Context getRemoteContext() throws NamingException {
        if(remoteContext == null) {
            Properties env = new Properties();
            env.setProperty(Context.PROVIDER_URL, providerUrl);
            env.setProperty(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
            if(securityPrincipal != null && !securityPrincipal.isEmpty()) {
                env.setProperty(Context.SECURITY_PRINCIPAL, securityPrincipal);
            }
            if(securityCredentials != null && !securityCredentials.isEmpty()) {
                env.setProperty(Context.SECURITY_CREDENTIALS, securityCredentials);
            }
            remoteContext = new InitialContext(env);
        }
        return remoteContext;
    }

    /**
     * Looks up an object with the specified name in the {@link
     * #getRemoteContext() remote context}
     *
     * @param name The name of the object in the remote context
     *
     * @return The remote object, or {@code null} if the object is not found
     *
     * @throws NamingException If there is an error accessing the remote context
     */
    private Object getRemoteObject(String name) throws NamingException {
        return getRemoteContext().lookup(name);
    }

    /**
     * Releases the connection to the remote naming context and frees any remote
     * resources held while the connection is open.
     */
    public void close() {
        if(remoteContext != null) {
            try {
                remoteContext.close();
            } catch(Exception t) {
                // swallow
            }
            remoteContext = null;
        }
    }

    /**
     * Implicitly invokes the {@link #close() close()} method upon garbage
     * collection to ensure that remote resources are freed.
     */
    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    /**
     * Returns a reference to a remote instance of the account session API.
     *
     * @return A reference to a remote instance of the account session API
     * @throws NamingException If there is an error resolving the remote object
     */
    public AttendanceService getAttendanceService() throws NamingException {
        return (AttendanceService)getRemoteObject(AttendanceService.JNDI_NAME);
    }

  
   
	
}