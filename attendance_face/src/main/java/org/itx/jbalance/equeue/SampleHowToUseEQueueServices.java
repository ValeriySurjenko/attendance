/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.itx.jbalance.equeue;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsResult;
import org.itx.jbalance.l2_api.dto.attendance.PermitDTO;
import org.itx.jbalance.l2_api.services.AttendanceService;

/**
 *
 * @author apv
 */
public class SampleHowToUseEQueueServices {
//    private String our_test_server = "jnp://localhost:1099";

    /**
     * Это наш тестовый сервак
     */
     private String our_test_server = "jnp://10.87.0.6:1099";
    
    
    public void test() throws NamingException{
        System.out.println("Hey you!");
        System.out.println("Это пример, как звать сервисы EQueue");
//        Создаем LookupHelper 
        final LookupHelper lookupHelper = new LookupHelper();
//        Указываем с каким сервером будем работать
//        По умолчанию localhost
//        На боевых серверах будет localhost всегда
        lookupHelper.setProviderUrl(our_test_server);
        final AttendanceService attendanceService =lookupHelper.getAttendanceService();
        
        final GetPermitsResult result = attendanceService.getPermits(new GetPermitsRequest());
        
//        Далее вывод того, что получили
        final List<PermitDTO> permits = result.getPermits();
        for (PermitDTO permit : permits) {
            System.out.println("Номер путевки: " + permit.getPermitNumber());
            System.out.println("Ребенок: " + permit.getChild().getFirstName());
            System.out.println("Дата: " + permit.getPermitDate());
            System.out.println("= = = =");
        }
        System.out.println("== END ==");
    }
    
    
    
    public static void main(String args[]){
        try {
            new SampleHowToUseEQueueServices().test();
        } catch (NamingException ex) {
            Logger.getLogger(SampleHowToUseEQueueServices.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
