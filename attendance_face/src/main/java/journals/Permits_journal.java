package journals;


import java.util.*;
import java.util.List;
import additional.Permit;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import org.itx.jbalance.equeue.LookupHelper;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsResult;
import org.itx.jbalance.l2_api.dto.attendance.PermitDTO;
import org.itx.jbalance.l2_api.services.AttendanceService;



public class Permits_journal {
    private List<PermitDTO> premits;
    private int page;
    
    private String filter_last_name;
    private String filter_first_name;
    private String filter_patronymic;
    private String filter_regnum;
    private Date filter_datereg;
    private int filter_datereg_direction;
    private int filter_birth_direction;
    private String filter_age;
    private Date filter_birth;
    private String filter_status;
    
    private String our_test_server = "jnp://10.87.0.6:1099";

    //******************************************
    public String getFilter_last_name() {
        return filter_last_name;
    }

    public void setFilter_last_name(String filter_last_name) {
        this.filter_last_name = filter_last_name;
    }

    public String getFilter_first_name() {
        return filter_first_name;
    }

    public void setFilter_first_name(String filter_first_name) {
        this.filter_first_name = filter_first_name;
    }

    public String getFilter_patronymic() {
        return filter_patronymic;
    }

    public void setFilter_patronymic(String filter_patronymic) {
        this.filter_patronymic = filter_patronymic;
    }

    public int getFilter_birth_direction() {
        return filter_birth_direction;
    }

    public void setFilter_birth_direction(int filter_birth_direction) {
        this.filter_birth_direction = filter_birth_direction;
    }

    public int getFilter_datereg_direction() {
        return filter_datereg_direction;
    }

    public void setFilter_datereg_direction(int filter_datereg_direction) {
        this.filter_datereg_direction = filter_datereg_direction;
    }

    public String getFilter_regnum() {
        return filter_regnum;
    }

    public void setFilter_regnum(String filter_regnum) {
        this.filter_regnum = filter_regnum;
    }

    public Date getFilter_datereg() {
        return filter_datereg;
    }

    public void setFilter_datereg(Date filter_datereg) {
        this.filter_datereg = filter_datereg;
    }

    public String getFilter_age() {
        return filter_age;
    }

    public void setFilter_age(String filter_age) {
        this.filter_age = filter_age;
    }

    public Date getFilter_birth() {
        return filter_birth;
    }

    public void setFilter_birth(Date filter_birth) {
        this.filter_birth = filter_birth;
    }

    public String getFilter_status() {
        return filter_status;
    }

    public void setFilter_status(String filter_status) {
        this.filter_status = filter_status;
    }
    
    //******************************************
    public boolean id_comp()
    {
        String num1;
        int num2;
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
        num1=params.get("num1");
        num2= Integer.getInteger(params.get("num2"));
        if(num1.isEmpty()==true) return true;
        int nm=Integer.parseInt(num1);
        if(nm==0)return true;
        if(nm==num2)return true;
        return false;
                
    }
    public boolean date_comp()  
    {
       Date d1,d2;
       int comp;
       FacesContext fc = FacesContext.getCurrentInstance();
       Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
       d1=new Date(params.get("d1"));
       d2=new Date(params.get("d2"));
       comp=Integer.getInteger(params.get("comp"));
       
       if (d1==null){return true;}
       switch (comp)
       {
           case 0:
               if ((d1.getDay()==d2.getDay())&&(d1.getMonth()==d2.getMonth())&&(d1.getYear()==d2.getYear())){return true;}
               break;
           case 1:
               if (d1.after(d2)){return true;}
               break;
           case 2:
               if (d1.before(d2)){return true;}
               break;

       }
        return false;
    }
    public List<PermitDTO> getPremits() {
        return premits;
    }

    public void setPremits(List<PermitDTO> premits) {
        this.premits = premits;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
    public boolean compare_id( Object cur)
    {
        String a;
        PermitDTO permit=(PermitDTO)cur;
        a=String.valueOf(permit.getUId());
        if(filter_regnum==null)return true;
        if(filter_regnum.length()==0)return true;
        if(a.startsWith(filter_regnum))return true;
        return false;
    }
     public boolean compare_last_name( Object cur)
    {
        String a;
        int b;
        if(filter_last_name==null)return true;
        PermitDTO permit=(PermitDTO)cur;
        a=permit.getChild().getLastName();
        return a.toLowerCase().startsWith(filter_last_name.toLowerCase());
        
    }
    public boolean compare_birth( Object cur)
    {
        Date a;
        int b;
      
        PermitDTO permit=(PermitDTO)cur;
        a=permit.getChild().getBirthday();
        if (filter_birth==null)return true;
        b=a.compareTo( filter_birth);
        switch (filter_birth_direction)
        {
            case 0:
                if (b==0)return true;
                break;
            case 1:
                if (b<0)return true;
                break;
            case 2:
                if (b>0)return true;
                  break;
        }
        return false;
    }
    public boolean compare_datereg( Object cur)
    {
        Date a;
        int b;
      
        PermitDTO permit=(PermitDTO)cur;
        a=permit.getPermitDate();
        if (filter_datereg==null)return true;
        b=a.compareTo( filter_datereg);
        switch (filter_datereg_direction)
        {
            case 0:
                if (b==0)return true;
                break;
            case 1:
                if (b<0)return true;
                break;
            case 2:
                if (b>0)return true;
                  break;
        }
        return false;
    }
    //******************************************
     public void load_permits() throws NamingException{
    
//        Создаем LookupHelper 
        final LookupHelper lookupHelper = new LookupHelper();
//        Указываем с каким сервером будем работать
//        По умолчанию localhost
//        На боевых серверах будет localhost всегда
        lookupHelper.setProviderUrl(our_test_server);
        final AttendanceService attendanceService =lookupHelper.getAttendanceService();
        
        final GetPermitsResult result = attendanceService.getPermits(new GetPermitsRequest());
        
//        Далее вывод того, что получили
        premits = result.getPermits();
      
    }
    public Permits_journal() 
    {
        
       Calendar d1;
       d1=new GregorianCalendar(2011,5,2);
       Date d2=d1.getTime();
       premits=new ArrayList<PermitDTO>();
       try
       {
            load_permits();
       }
       catch(NamingException e)
       {
           
       }
      
    }
}
