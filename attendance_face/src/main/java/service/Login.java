/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.context.FacesContext;
import java.util.Map;

/**
 *
 * @author admin
 */
public class Login {
    private String curpage;
    private String username;
    private String pass;
    private boolean logined;
    //***************************************
    private boolean lazy_logined;
    private boolean lazy_user;
    
    //***************************************
    public boolean isLazy_logined() {
        check_logined();
        return lazy_logined;
    }

    public void setLazy_logined(boolean lazy_logined) {
        this.lazy_logined = lazy_logined;
    }

    public boolean isLazy_user() {
        check_user();
        return lazy_user;
    }

    public void setLazy_user(boolean lazy_user) {
        this.lazy_user = lazy_user;
    }
    
    //***************************************
    public String getCurpage() {
        return curpage;
    }

    public void setCurpage(String curpage) {
        this.curpage = curpage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isLogined() {
        return logined;
    }

    public void setLogined(boolean logined) {
        this.logined = logined;
    }

    //***************************************
 
    public void curpagelistener ()
    {

        String b;
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
        
        curpage=params.get("path");
        setCurpage(params.get("path"));
    }
    public String check_user()
    {
          curpage="about.xhtml";
        logined=true;
        try
        {
        FacesContext.getCurrentInstance().getExternalContext().redirect("faces/index.xhtml");
        }
        catch(Exception ex)
        {
                        
        }
        return "index.xhtml";
    }
    public void check_logined()
    {
        if(logined==false)
        {
            try
            {
                FacesContext.getCurrentInstance().getExternalContext().redirect("faces/login.xhtml");
            }
            catch(Exception ex)
            {
                        
            }
        }
    }
    //***************************************
    public Login() 
    {
        logined=false;
        curpage="welcome.xhtml";
    }
}
