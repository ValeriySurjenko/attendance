
package catalogs;

/**
 *
 * @author Валерий Сурженко
 */

import java.util.*;
import java.util.List;
import additional.AgeGroup;


public class AgeGroup_catalog {
    private List<additional.AgeGroup> agegroups;
    private Integer page;
//----------------------------------------------------------
    public List<AgeGroup> getAgegroups() {
        return agegroups;
    }

    public void setAgegroups(List<AgeGroup> agegroups) {
        this.agegroups = agegroups;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
    
//----------------------------------------------------------    
    public AgeGroup_catalog() 
    {
        agegroups=new ArrayList<additional.AgeGroup>();
        agegroups.add(new AgeGroup(" Группа раннего возраста",(float)1.6,(float)3.0));
        agegroups.add(new AgeGroup("Вторая младшая группа",(float)3.1,(float)4.0));
        agegroups.add(new AgeGroup("Средняя группа",(float)4.1,(float)5));
        agegroups.add(new AgeGroup("Старшая группа",(float)5.1,(float)6));
        agegroups.add(new AgeGroup("Логопедическая старшая группа ",(float)5.1,(float)6));
        agegroups.add(new AgeGroup("Подготовительная группа ",(float)6.1,(float)7));
        agegroups.add(new AgeGroup("Логопедическая подготовительная группа",(float)6.1,(float)7));
        agegroups.add(new AgeGroup("Разновозрастная гр. младшего дошкольного возраста",(float)1.6,(float)4));
        agegroups.add(new AgeGroup("Разновозрастная гр. старшего дошкольного возраста",(float)4.0,(float)7));
    }
    
}
